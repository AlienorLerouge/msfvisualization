import msf

# Single
path_param = (r'./result/', 'scenario', 'ITA', '120m')
res = msf.MSFResults('desc_tables.json', 'baseline', *path_param)
res.demography_population_pyramid(2010, 2030)
res.health_plot_diseases_new_case()


interventions = ('food-labelling', 'menu-labelling', 'mass-media')
res_multi = msf.MSFCompareMulti('desc_tables.json', 'baseline', interventions, *path_param)
res_multi.table_daly()