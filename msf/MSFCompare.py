from msf import tools
from msf import plot

import pandas as pd
import numpy as np

import json
import re



class MSFCompareSingle:
    def __init__(self, json_tables_desc, label_0, label_1, *path_parameters):
        self._path, self._path_i = tools.get_paths(label_0, label_1, *path_parameters)
        with open(json_tables_desc) as data_file:
            self._table_desc = json.load(data_file)

    def load_file(self, csv):
        baseline = pd.read_csv(self._path + csv)
        intervention = pd.read_csv(self._path_i + csv)
        return baseline, intervention

    def compute_diff_table(self, table_name, dimensions=[], year_range=None):
        baseline, intervention = self.load_file(table_name)
        if year_range is not None:
            baseline = baseline.loc[baseline['year'].isin(year_range)]
            intervention = intervention.loc[intervention['year'].isin(year_range)]

        dimensions_base = []
        for dim in baseline.axes[1].base:
            if not re.search('sample[0-9]+', dim):
                dimensions_base.append(dim)

        # Ajout / modification des dimensions selon besoin
        # Si besoin on rajoute les dimensions suivantes 'AgeGroupx10', et 'AgeGroupx5'
        if 'AgeGroup_10x' in dimensions:
            tools.create_age_group_10x(baseline, 'AgeGroup_10x')
            tools.create_age_group_10x(intervention, 'AgeGroup_10x')
            dimensions_base.append('AgeGroup_10x')

        if 'AgeGroup_5x' in dimensions:
            tools.create_age_group_5x(baseline, 'AgeGroup_5x')
            tools.create_age_group_5x(intervention, 'AgeGroup_5x')
            dimensions_base.append('AgeGroup_5x')

        if dimensions == []:
            baseline = pd.DataFrame(baseline.sum(), columns=['value']).T
            intervention = pd.DataFrame(intervention.sum(), columns=['value']).T
        else:
            baseline = baseline.groupby(dimensions).sum()
            intervention = intervention.groupby(dimensions).sum()

        for dim in dimensions_base:
            if dim not in dimensions:
                baseline = baseline.drop([dim], axis=1)
                intervention = intervention.drop([dim], axis=1)

        diff = baseline - intervention

        # print(diff.T)
        # mean_b = pd.DataFrame(baseline.mean(axis=1), columns=['baseline'])
        # mean_i = pd.DataFrame(intervention.mean(axis=1), columns=['intervention'])

        nb_subsamples = diff.shape[1]
        mean = pd.DataFrame(diff.mean(axis=1), columns=['mean'])
        std = pd.DataFrame(diff.std(axis=1), columns=['c95'])
        std = std / np.sqrt(nb_subsamples - 1)

        std['c95'] = std * 1.64

        res = pd.concat([mean, std], axis=1)
        res.reset_index(level=None, inplace=True)

        # Relabelling dimensions if needed
        tables_dimensions = self._table_desc[table_name]['dimensions']
        for dim in tables_dimensions:
            if dim['name'] in dimensions and dim["type"] == 'index':
                tools.relabel_dimension(res, dim['name'], dim['ids'])

        return res


class MSFCompareMulti:
    def __init__(self, json_tables_desc, baseline_label, intervention_labels, *path_parameters):
        self._compare = {intervention : MSFCompareSingle(json_tables_desc, baseline_label, intervention, *path_parameters)
                         for intervention in intervention_labels}

    def table_daly(self):
        frames = []
        for intervention in self._compare:
            compare_single = self._compare[intervention]

            table_daly = compare_single.compute_diff_table('DALY.csv', ['gender'])
            table_py = compare_single.compute_diff_table('PersonYears.csv', ['gender'])
            table_daly = table_daly.rename(columns={'mean': 'DALYs', 'c95': 'error(DALYs)'})
            table_py = table_py.rename(columns={'mean': 'PersonYears', 'c95': 'error(PYs)'})
            res = pd.merge(table_daly, table_py, on='gender')
            res['intervention'] = intervention
            frames += [res]
        result = pd.concat(frames)
        print(result)
        return result


    # data = google.visualization.arrayToDataTable([
    #     ['Intervention', 'Person-Years', 'Daly'],
    # ]);