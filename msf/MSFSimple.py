from msf import tools
from msf import plot

import pandas as pd
import numpy as np

import json
import re



class MSFResults:
    def __init__(self, json_tables_desc, label, *path_parameters):
        self._path = tools.get_path(label, *path_parameters)
        with open(json_tables_desc) as data_file:
            self._table_desc = json.load(data_file)

    def load_file(self, csv):
        baseline = pd.read_csv(self._path + csv)
        return baseline

    def compute_table(self, table_name, dimensions=[], year_range=None):
        baseline = self.load_file(table_name)
        if year_range is not None:
            baseline = baseline.loc[baseline['year'].isin(year_range)]

        # Every table has the same format
        # Dim1, Dim2,.., DimN, Mean, StdError, sample0, sample1, ...., sampleM
        # The idea is to aggregate on the desired dimensions every samples and the to compute
        # and with the aggregated samples compute the mean and the std_dev

        # La dimensions_base c'est Dim1, Dim2,.., DimN, Mean, StdError
        dimensions_base = []
        for dim in baseline.axes[1].base:
            if not re.search('sample[0-9]+', dim):
                dimensions_base.append(dim)

        # Ajout / modification des dimensions selon besoin
        # Si besoin on rajoute les dimensions suivantes 'AgeGroupx10', et 'AgeGroupx5'
        if 'AgeGroup_10x' in dimensions:
            tools.create_age_group_10x(baseline, 'AgeGroup_10x')
            dimensions_base.append('AgeGroup_10x')

        if 'AgeGroup_5x' in dimensions:
            tools.create_age_group_5x(baseline, 'AgeGroup_5x')
            dimensions_base.append('AgeGroup_5x')

        # Using a groupBy to summarize the  Table
        if not dimensions:
            baseline = pd.DataFrame(baseline.sum(), columns=['value']).T
        else:
            baseline = baseline.groupby(dimensions).sum()

        # Dropping other dimensions to compute correctly the mean on all the columns
        for dim in dimensions_base:
            if dim not in dimensions:
                baseline = baseline.drop([dim], axis=1)

        # Mean and Std computation
        nb_subsamples = baseline.shape[1]
        if not nb_subsamples:
            raise TypeError('No subsamples in results')

        mean = pd.DataFrame(baseline.mean(axis=1), columns=['mean'])
        std = pd.DataFrame(baseline.std(axis=1), columns=['std_error'])
        std = std / np.sqrt(nb_subsamples - 1)
        res = pd.concat([mean, std], axis=1)


        # we reindex the dataFrame (which is still groupBy to get a flat DataFrame)
        res.reset_index(inplace=True)
        res['c95'] = res['std_error'] * 1.64

        # Relabelling dimensions if needed
        tables_dimensions = self._table_desc[table_name]['dimensions']
        for dim in tables_dimensions:
            if dim['name'] in dimensions and dim["type"] == 'index':
                tools.relabel_dimension(res, dim['name'], dim['ids'])


        return res

    def demography_population_pyramid(self, first_year, last_year, filename=None):
        population = self.compute_table('Population.csv', ['year', 'gender', 'AgeGroup_10x'])
        plot.plot_table_population_pyramid_evolution(population, first_year, last_year, filename)

    def health_plot_diseases_new_case(self, filename=None):
        new_cases = self.compute_table('DiseaseOccurence.csv', ['year', 'disease'])
        plot.plot_disease_incidence(new_cases, filename)