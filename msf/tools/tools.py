import os
import pandas as pd
import sqlite3
import numpy as np


def get_path(label, *path_parameters):
    res_path, scenario, country, nb_sim = path_parameters
    path_0 = res_path + scenario + '/' + country + '/' + label + '-' + str(nb_sim) + '/'
    return path_0

def get_paths(label_0, label_1, *path_parameters):
    return get_path(label_0, *path_parameters), get_path(label_1, *path_parameters)


# def load_result_data(release_path, country, scenario, table_name):
#     data_path = last_result_directory(country, release_path + 'result\\', scenario)
#     data_frame = pd.read_csv(data_path + table_name)
#     data_frame = data_frame.rename(columns={'value': 'mean'})
#     return data_frame


def label_age_group_10x():
    labels = ["{0} - {1}".format(i, i + 9) for i in range(0, 90, 10)]
    labels.append("90+")
    return labels


def create_age_group_10x(dataframe, columnName = 'AgeGroup_10x'):
    labels = label_age_group_10x()
    age_cut = list(range(0, 91, 10)) + [111]
    dataframe[columnName] = pd.cut(dataframe.age, age_cut, right=False, labels=labels)
    return labels


def label_age_group_5x():
    labels = ["{0} - {1}".format(i, i + 4) for i in range(0, 90, 5)]
    labels.append("90+")
    return labels


def create_age_group_5x(dataframe, columnName='AgeGroup_5x'):
    labels = label_age_group_5x()
    age_cut = list(range(0, 91, 5)) + [111]
    dataframe[columnName] = pd.cut(dataframe.age, age_cut, right=False, labels=labels)
    return labels


def relabel_dimension(dataframe, dimension, ids):
    new_ids = {int(id): ids[id] for id in ids}
    try:
        dataframe[dimension] = dataframe.apply(lambda x: new_ids[x[dimension]], axis=1)
    except KeyError:
        print('Unconsistent relabeling')

def thousands(x, pos):
    'The two args are the value and tick position'
    return '%1.fk' % (x*1e-3)


def millions(x, pos):
    'The two args are the value and tick position'
    return '%1.1fM' % (x*1e-6)
