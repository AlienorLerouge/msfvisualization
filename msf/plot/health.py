import pandas as pd
import numpy as np
# Matplotlib
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter


# Pyplot options
font = {'size'   : 12}
matplotlib.rc('font', **font)
matplotlib.rcParams['figure.figsize'] = (5.0, 5.0)
plt.style.use('ggplot')

import seaborn as sns

# df_population Dataframe : 'disease', 'year','mean', 'std'
def table_disease_incidence(df_incidence_cases):
    pv = df_incidence_cases.pivot_table(index='year', columns='disease', values='mean')
    return pv

def plot_disease_incidence(df_incidence_cases, filename='None'):
    pv = table_disease_incidence(df_incidence_cases)
    fig, axes = plt.subplots(figsize=(14,7))
    legend_boxes = ()
    disease_names = [d for d in pv.columns if d != 'Back pain']
    for disease in disease_names:
        line = axes.plot(pv[disease], label='disease')
        legend_boxes += (line[0], )

    fig.legend(legend_boxes,
               tuple(disease_names),
               fontsize=12,  ncol=5, loc='lower center')


    axes.set_title('Incidence Cases per Year')

    if filename:
        plt.savefig(filename, bbox_inches='tight')

    plt.show()
