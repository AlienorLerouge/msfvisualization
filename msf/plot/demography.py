import pandas as pd
import numpy as np
# Matplotlib
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter


# Pyplot options
font = {'size'   : 12}
matplotlib.rc('font', **font)
matplotlib.rcParams['figure.figsize'] = (5.0, 5.0)
plt.style.use('ggplot')

import seaborn as sns

# df_population Dataframe : 'gender', 'year', 'AgeGroup_10x', 'mean', 'std'
def table_population_pyramid_evolution(df_population, first_year, second_year):
    df_population = df_population.groupby(['year', 'gender', 'AgeGroup_10x']).sum()
    def create_gender_series(gender):
        sy = pd.Series(df_population['mean'].loc[first_year, gender], name=first_year)
        ly = pd.Series(df_population['mean'].loc[second_year, gender], name=second_year)
        return  pd.concat([sy, ly], axis=1)

    female_pop = create_gender_series('Female')
    male_pop = create_gender_series('Male')
    return female_pop, male_pop
    # I imagine that those tables could then be used to send data to a google charts or something else

def  plot_table_population_pyramid_evolution(df_population, first_year, second_year, filename=None):
    female_pop, male_pop = table_population_pyramid_evolution(df_population, first_year, second_year)

    y_label = female_pop.index.categories
    y = np.arange(len(y_label))

    width = 0.9
    fig, axes = plt.subplots(ncols=2, sharey=True, figsize=(7, 6))
    f_st = axes[0].barh(y, female_pop[first_year].values, width / 2, color=sns.xkcd_rgb["mid blue"], zorder=10)
    f_ly = axes[0].barh(y + width / 2, female_pop[second_year].values, width / 2, color=sns.xkcd_rgb["pale teal"], zorder=10)
    m_st = axes[1].barh(y, male_pop[first_year].values, width / 2, color=sns.xkcd_rgb["pale red"])
    m_ly = axes[1].barh(y + width / 2, male_pop[second_year].values, width / 2, color=sns.xkcd_rgb["melon"])

    fig.legend((f_st[0], f_ly[0], m_st[0], m_ly[0]),
               ('Female ' + str(first_year),
                'Female ' + str(second_year),
                'Male' + str(first_year),
                'Male' + str(second_year))
               , fontsize=12,
               ncol=4, loc='lower center')

    axes[0].set_title('Female', fontsize=14)
    axes[1].set_title('Male', fontsize=14)

    #         axes[1].set(title=last_year, fontsize=14)

    axes[0].invert_xaxis()
    axes[0].set(yticks=y, yticklabels=y_label)
    axes[0].set_yticks(y + 0.5 * width)
    # axes[0].yaxis.tick_right()

    fmillion = lambda x, y: '0' if x == 0 else '%1.0fM' % (x * 1e-6)
    formatter = FuncFormatter(fmillion)
    axes[0].xaxis.set_major_formatter(formatter)
    axes[1].xaxis.set_major_formatter(formatter)
    for ax in axes.flat:
        ax.margins(0.03)
        ax.grid(True)

    fig.subplots_adjust(wspace=0.05)

    if filename:
        plt.savefig(filename, bbox_inches='tight')

    plt.show()